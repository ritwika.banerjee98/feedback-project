
<html> 
<head>
 <title>Sign-In</title>

 <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 </head>
 <!-- <body id="body-color"> 
 <div id="Sign-In"> 
 <fieldset style="width:30%">
 <legend>FEEDBACK PAGE LOG-IN </legend> 
 <form method="POST" action="connectivity.php"> 
 User <br><input type="text" name="user" size="40">
 <br> Password <br><input type="password" name="pass" size="40"><br>
 <br> 
 <input type="radio" name="login_type" value="1">Student<br>
<input type="radio" name="login_type" value="2">Teacher<br>
<input type="radio" name="login_type" value="3">Admin<br><br>
 <input id="button" type="submit" name="submit" value="Log-In">
 </form>
 </fieldset> 
 </div>  -->

 <div id="login">
        <h3 class="text-center text-white pt-5">Department Of Information Technology</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" method="POST" action="connectivity.php">
                            <h3 class="text-center text-info">Login</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <input type="text" name="user" size="40" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password"  name="pass" size="40" class="form-control">
                            </div>
                            <br><br><br>
                            <div id="register-link" class="text-center">
                            <input type="radio" name="login_type" value="1"> Student
                            <input type="radio" name="login_type" value="2"> Teacher
                            <input type="radio" name="login_type" value="3"> Admin<br>
                            </div>
                            <br>
                            <div class="form-group">
                              
                               <center> <input id="button" type="submit" name="submit" class="btn btn-info btn-md" value="submit"></center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body> </html>
<style>
body {
  margin: 0;
  padding: 0;
  background-color: #17a2b8;
  height: 100vh;
}
#login .container #login-row #login-column #login-box {
  margin-top: 120px;
  max-width: 600px;
  height: 320px;
  border: 1px solid #9C9C9C;
  background-color: #EAEAEA;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 20px;
}
#login .container #login-row #login-column #login-box #login-form #register-link {
  margin-top: -85px;
}
</style>


!