-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 04, 2019 at 07:14 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `misfeedback`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_no` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_no`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `batch_master`
--

CREATE TABLE `batch_master` (
  `batch_id` int(20) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `feedback_no` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_master`
--

INSERT INTO `batch_master` (`batch_id`, `batch_name`, `feedback_no`) VALUES
(1, '2015', 1),
(2, '2016', 1),
(3, '2017', 1),
(4, '2018', 1);

-- --------------------------------------------------------

--
-- Table structure for table `branch_master`
--

CREATE TABLE `branch_master` (
  `b_id` int(20) NOT NULL,
  `b_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch_master`
--

INSERT INTO `branch_master` (`b_id`, `b_name`) VALUES
(1, 'IT');

-- --------------------------------------------------------

--
-- Table structure for table `division_master`
--

CREATE TABLE `division_master` (
  `id` int(10) NOT NULL,
  `division` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division_master`
--

INSERT INTO `division_master` (`id`, `division`) VALUES
(1, 'Class A'),
(2, 'Class B');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_master`
--

CREATE TABLE `faculty_master` (
  `f_id` int(20) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `b_id` int(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `batch_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_master`
--

INSERT INTO `faculty_master` (`f_id`, `f_name`, `l_name`, `b_id`, `password`, `batch_id`) VALUES
(1, 'Jhuma', 'Ray', 1, 'faculty', NULL),
(2, 'Soumyadip ', 'Dhar', 1, 'faculty', NULL),
(3, 'Jayanta ', 'Dutta', 1, 'faculty', NULL),
(4, 'Hiranmoy', 'Roy', 1, 'faculty', NULL),
(5, 'Amit', 'Khan', 1, 'faculty', NULL),
(6, 'Abantika', 'Chowdhury', 1, 'faculty', NULL),
(7, 'Indrajit', 'Pan', 1, 'faculty', NULL),
(8, 'Abhijit', 'Das', 1, 'faculty', NULL),
(9, 'Anirban', 'Mukherjee', 1, 'faculty', NULL),
(10, 'Sudarshan', 'Biswas', 1, 'faculty', NULL),
(11, 'Hrishikesh ', 'Bhowmik', 1, 'faculty', NULL),
(12, 'Pankaj', 'Pal', 1, 'faculty', NULL),
(13, 'Dipankar ', 'Majumdar', 1, 'faculty', NULL),
(14, 'M. ', 'Jana', 1, 'faculty', NULL),
(15, 'Sutapa ', 'Ghosh', 1, 'faculty', NULL),
(16, 'Sharmistha ', 'Chatterjee', 1, 'faculty', NULL),
(17, 'Ranjan', 'Jana', 1, 'faculty', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback_master`
--

CREATE TABLE `feedback_master` (
  `feed_id` int(20) NOT NULL,
  `roll_no` varchar(255) NOT NULL,
  `b_id` int(20) NOT NULL,
  `batch_id` int(20) NOT NULL,
  `feedback_no` int(20) NOT NULL,
  `sem_id` int(20) NOT NULL,
  `f_id` int(20) NOT NULL,
  `sub_id` int(20) NOT NULL,
  `division_id` int(10) NOT NULL,
  `ans1` int(20) NOT NULL,
  `ans2` int(20) NOT NULL,
  `ans3` int(20) NOT NULL,
  `ans4` int(20) NOT NULL,
  `ans5` int(20) NOT NULL,
  `ans6` int(20) NOT NULL,
  `ans7` int(20) NOT NULL,
  `ans8` int(20) NOT NULL,
  `ans9` int(20) NOT NULL,
  `remark` text NOT NULL,
  `feed_date` date NOT NULL,
  `ans10` int(20) DEFAULT NULL,
  `average` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback_master`
--

INSERT INTO `feedback_master` (`feed_id`, `roll_no`, `b_id`, `batch_id`, `feedback_no`, `sem_id`, `f_id`, `sub_id`, `division_id`, `ans1`, `ans2`, `ans3`, `ans4`, `ans5`, `ans6`, `ans7`, `ans8`, `ans9`, `remark`, `feed_date`, `ans10`, `average`) VALUES
(1, 'IT2016010', 1, 2, 1, 6, 1, 1, 1, 2, 2, 2, 3, 2, 3, 2, 3, 2, 'Hey@', '2019-03-03', 5, 26),
(2, 'IT2016002', 1, 2, 1, 6, 1, 1, 1, 5, 5, 5, 5, 5, 5, 5, 5, 5, 'You are', '2019-03-04', 3, 48),
(3, 'IT2016006', 1, 2, 1, 6, 1, 1, 1, 2, 2, 2, 2, 2, 3, 2, 1, 4, 'Bad', '2019-03-04', 2, 22),
(5, 'IT2016008', 1, 2, 1, 6, 1, 1, 1, 5, 9, 7, 9, 8, 9, 5, 4, 8, '', '2019-03-04', 8, 7),
(6, 'IT2016010', 1, 2, 1, 6, 2, 2, 1, 2, 3, 5, 5, 3, 9, 9, 7, 8, '', '2019-03-04', 2, 53);

-- --------------------------------------------------------

--
-- Table structure for table `feedback_para`
--

CREATE TABLE `feedback_para` (
  `para_id` int(1) NOT NULL,
  `batch_id` int(10) NOT NULL,
  `b_id` int(10) NOT NULL,
  `sem_id` int(10) NOT NULL,
  `division_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback_para`
--

INSERT INTO `feedback_para` (`para_id`, `batch_id`, `b_id`, `sem_id`, `division_id`) VALUES
(1, 1, 1, 8, 1),
(2, 2, 1, 6, 1),
(3, 3, 1, 4, 1),
(4, 4, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `feedback_ques_master`
--

CREATE TABLE `feedback_ques_master` (
  `q_id` int(20) NOT NULL,
  `ques` varchar(255) NOT NULL,
  `one_word` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback_ques_master`
--

INSERT INTO `feedback_ques_master` (`q_id`, `ques`, `one_word`) VALUES
(1, 'Faculty was punctual in class.', 'Punctual '),
(2, 'Faculty was well prepared for the classes.', 'Well_prepared'),
(3, 'Faculty communication skill were good.', 'Communication'),
(4, 'Teaching methodology was good.', 'Methodology '),
(5, 'Faculty had clearly defined objectives for each class.', 'Defined_objectives'),
(6, 'Faculty was able to establish the need of this subject.', 'Growing interest'),
(7, 'Faculty treated me with respect and aided in my learning.', 'Respected'),
(8, 'Faculty was instrumental in the value addition process.', 'Instrumental_use'),
(9, 'Faculty adequately cleared all my doubts and was helpful in solving queries.', 'Solving queries'),
(10, 'In my opinion the same faculty should be continued for such subjects.', 'Be_Continued');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`username`, `password`) VALUES
('admin', '*00A51F3F48415C7D4E8908980D443C29C69B60C9');

-- --------------------------------------------------------

--
-- Table structure for table `semester_master`
--

CREATE TABLE `semester_master` (
  `sem_id` int(20) NOT NULL,
  `sem_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester_master`
--

INSERT INTO `semester_master` (`sem_id`, `sem_name`) VALUES
(1, 'I'),
(2, 'II'),
(3, 'III'),
(4, 'IV'),
(5, 'V'),
(6, 'VI'),
(7, 'VII'),
(8, 'VIII');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `roll_no` varchar(255) NOT NULL,
  `password` varchar(30) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `b_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`roll_no`, `password`, `Name`, `batch_id`, `b_id`) VALUES
('IT2015001', 'IT2015001', '', 1, 1),
('IT2015002', 'IT2015002', '', 1, 1),
('IT2015003', 'IT2015003', '', 1, 1),
('IT2015004', 'IT2015004', '', 1, 1),
('IT2015005', 'IT2015005', '', 1, 1),
('IT2015006', 'IT2015006', '', 1, 1),
('IT2015007', 'IT2015007', '', 1, 1),
('IT2015008', 'IT2015008', '', 1, 1),
('IT2015009', 'IT2015009', '', 1, 1),
('IT2015010', 'IT2015010', '', 1, 1),
('IT2016001', 'IT2016001', '', 2, 1),
('IT2016002', 'IT2016002', '', 2, 1),
('IT2016003', 'IT2016003', '', 2, 1),
('IT2016004', 'IT2016004', '', 2, 1),
('IT2016005', 'IT2016005', '', 2, 1),
('IT2016006', 'IT2016006', '', 2, 1),
('IT2016007', 'IT2016007', '', 2, 1),
('IT2016008', 'IT2016008', '', 2, 1),
('IT2016009', 'IT2016009', '', 2, 1),
('IT2016010', 'IT2016010', '', 2, 1),
('IT2017001', 'IT2017001', '', 3, 1),
('IT2017002', 'IT2017002', '', 3, 1),
('IT2017003', 'IT2017003', '', 3, 1),
('IT2017004', 'IT2017004', '', 3, 1),
('IT2017005', 'IT2017005', '', 3, 1),
('IT2017006', 'IT2017006', '', 3, 1),
('IT2017007', 'IT2017007', '', 3, 1),
('IT2017008', 'IT2017008', '', 3, 1),
('IT2017009', 'IT2017009', '', 3, 1),
('IT2017010', 'IT2017010', '', 3, 1),
('IT2018001', 'IT2018001', '', 4, 1),
('IT2018002', 'IT2018002', '', 4, 1),
('IT2018003', 'IT2018003', '', 4, 1),
('IT2018004', 'IT2018004', '', 4, 1),
('IT2018005', 'IT2018005', '', 4, 1),
('IT2018006', 'IT2018006', '', 4, 1),
('IT2018007', 'IT2018007', '', 4, 1),
('IT2018008', 'IT2018008', '', 4, 1),
('IT2018009', 'IT2018009', '', 4, 1),
('IT2018010', 'IT2018010', '', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subject_master`
--

CREATE TABLE `subject_master` (
  `sub_id` int(20) NOT NULL,
  `sub_name` varchar(255) NOT NULL,
  `sem_id` int(20) NOT NULL,
  `f_id` int(20) NOT NULL,
  `batch_id` int(20) NOT NULL,
  `division_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_master`
--

INSERT INTO `subject_master` (`sub_id`, `sub_name`, `sem_id`, `f_id`, `batch_id`, `division_id`) VALUES
(1, 'Principles of Management(HU601)', 6, 1, 2, 1),
(2, 'DBMS (IT-601)', 6, 2, 2, 1),
(3, 'DBMS LAB(IT-691)', 6, 3, 2, 1),
(4, 'Computer Networking (IT-602)', 6, 4, 2, 1),
(5, 'Computer Networking Lab(IT-692)', 6, 4, 2, 1),
(6, 'Software Engg.(IT-603)', 6, 5, 2, 1),
(7, 'Software Engg. Lab(IT-693)', 6, 5, 2, 1),
(8, 'ERP (IT-604D)', 6, 6, 2, 1),
(9, 'Artificial Intelligence(IT-605D)', 6, 7, 2, 1),
(10, 'Seminar(IT-681)', 6, 8, 2, 1),
(11, 'Organisational Behaviour(HU801-A)', 8, 9, 1, 1),
(12, 'Cryptography and Network Security(IT-801D)', 8, 10, 1, 1),
(13, 'Cyber Law and Security Pol(IT-802B)', 8, 11, 1, 1),
(14, 'Design Lab(IT-891)', 8, 6, 1, 1),
(15, 'Numerical Method Theory(M(CS)401)', 4, 2, 3, 1),
(16, 'Numerical Method Lab(M(CS)491)', 4, 2, 3, 1),
(17, 'Mathematics3(M401)', 4, 14, 3, 1),
(18, 'Comm. Engg. & Coding Theory(CS401)', 4, 12, 3, 1),
(19, 'Comm. Engg. & Coding Lab(CS491)', 4, 12, 3, 1),
(20, 'Formal Lang. & Auto. Th(CS402)', 4, 13, 3, 1),
(21, 'Obj. Prog. & UML Theory(IT401)', 4, 13, 3, 1),
(22, 'Obj. Prog. & UML Lab(IT481)', 4, 13, 3, 1),
(23, 'Software Tools(CS492)', 4, 10, 3, 1),
(24, 'Tech. Report Writing(HU481)', 4, 15, 3, 1),
(25, 'Programming For Problem Solving', 2, 17, 4, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_no`);

--
-- Indexes for table `batch_master`
--
ALTER TABLE `batch_master`
  ADD PRIMARY KEY (`batch_id`);

--
-- Indexes for table `branch_master`
--
ALTER TABLE `branch_master`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `division_master`
--
ALTER TABLE `division_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_master`
--
ALTER TABLE `faculty_master`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `feedback_master`
--
ALTER TABLE `feedback_master`
  ADD PRIMARY KEY (`feed_id`);

--
-- Indexes for table `feedback_para`
--
ALTER TABLE `feedback_para`
  ADD PRIMARY KEY (`para_id`);

--
-- Indexes for table `feedback_ques_master`
--
ALTER TABLE `feedback_ques_master`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `semester_master`
--
ALTER TABLE `semester_master`
  ADD PRIMARY KEY (`sem_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`roll_no`);

--
-- Indexes for table `subject_master`
--
ALTER TABLE `subject_master`
  ADD PRIMARY KEY (`sub_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `batch_master`
--
ALTER TABLE `batch_master`
  MODIFY `batch_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `branch_master`
--
ALTER TABLE `branch_master`
  MODIFY `b_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `division_master`
--
ALTER TABLE `division_master`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faculty_master`
--
ALTER TABLE `faculty_master`
  MODIFY `f_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `feedback_master`
--
ALTER TABLE `feedback_master`
  MODIFY `feed_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `feedback_para`
--
ALTER TABLE `feedback_para`
  MODIFY `para_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `feedback_ques_master`
--
ALTER TABLE `feedback_ques_master`
  MODIFY `q_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `semester_master`
--
ALTER TABLE `semester_master`
  MODIFY `sem_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subject_master`
--
ALTER TABLE `subject_master`
  MODIFY `sub_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
