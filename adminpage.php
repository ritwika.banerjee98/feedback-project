<?php
session_start();  
include("includes/config_db.php");
//include("ajax_script.php");
$default = 1;
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "misfeedback";

// Create connection
 $conn = mysqli_connect($servername, $username, $password, $dbname);
 if($_SESSION['user']==''){
	header("location: index.php");
}

?>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="includes/style1.css" />
<style>
		td, th {
    /* padding: 0; */
    padding-left: 40px;
}
</style>
</head>

<body class="body">
<nav class="navbar navbar-inverse navbar-fixed-top" style="padding-right:5px;border-radius:0px;">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">RCCIIT - IT</a>
    </div>
		<form action="logoutHandler.php" method="post">
    <ul class="nav navbar-nav navbar-right">
		<li ><a href="#">Hey, <?php echo $_SESSION['user']; ?></a></li>
			<li class="active"><a href="#">Home</a></li>
			<li><a href="changePassword.php">Change Password</a></li>
			
			<button name="logout" class="btn btn-danger navbar-btn">Logout</button>
		
		
    </ul>
		</form>
  
  </div>
</nav>
<table style="margin-top:50px;" width="850" height="561"  border="0" align="center" cellpadding="5" cellspacing="5" >
<tr>
	<td class="text-info" width="650"  valign="bottom" align="center"><p><b><font size="7" >Admin Page</font></b></p></td>
  </tr>
  <tr>
    <td width="400" height="126" valign=top>
        <form action="" method="post" name="feedback_result" onsubmit="return chkForm();">
            
      <table width="601" border="0" align="center">        
        <tr>
          <!-- <td width="1">&nbsp;</td> -->
          <!--<td width="98">Date</td>
          <td width="175"><label>
            <input type="text" name="date" id="date" readonly tabindex="2"/><a href="javascript:viewcalendar()">cal</a>
          </label></td>-->
		  
        </tr>
          <tr>


			<!-- ***********************BATCH --> 	
		  <td align="center" width="161"><h4>Batch :</h4></td>
		  <td width="173"><label>
      <?php
			$sel_batch="select * from batch_master";
			$res_batch=mysqli_query($conn,$sel_batch);
			
			 while($batch_combo=mysqli_fetch_array($res_batch))
			 {							
				$bat_combo[] = array('id' => $batch_combo['batch_id'],
									   'text' => $batch_combo['batch_name']);								  
			 }
			 echo tep_draw_pull_down_menu('batch_name',$bat_combo,$default,' tabindex="2" ');
			
			$sel_para="select * from feedback_para";
			$res_para=mysqli_query($conn,$sel_para);
			$result_para=mysqli_fetch_array($res_para);
			
                        $sel_para1="select * from student where roll_no='".$_SESSION['user']."'";
			$res_para1=mysqli_query($conn,$sel_para1);
			$result_para1=mysqli_fetch_array($res_para1);
			?>
            <input type="hidden"  value="<?php echo $result_para1['batch_id']?>"/>
			<?php //echo batch_name($result_para['batch_id']);?>
          </label>
		</td>
		  <td>&nbsp;</td>
          <td> </td>
          <td><label>
            <?php
			$sel_b="select * from branch_master";
			$res=mysqli_query($conn,$sel_b);
			
			 while($b_combo=mysqli_fetch_array($res))
			 {							
				$branch_combo[] = array('id' => $b_combo['b_id'],
									    'text' => $b_combo['b_name']);								  
			 }
			 //echo tep_draw_pull_down_menu('b_name',$branch_combo,$default,' tabindex="3" ');
			?>
            <input type="hidden" name="b_name" value="<?php echo $result_para['b_id']?>"/>
			
          </label></td>
          
          			          
        </tr>
		
		<tr>
		<td></td>
          <td>
		  <?php
			 $sel_sem="select * from semester_master ";
			 $res_sem=mysqli_query($conn,$sel_sem);
			
			 while($sem_combo=mysqli_fetch_array($res_sem))
			 {							
				$sem_array[] = array('id' => $sem_combo['sem_id'],
									 'text' => $sem_combo['sem_name']);								  
			 }
			 
			 //echo tep_draw_pull_down_menu('sem_name',$sem_array,$default,' tabindex="4" ');
	      ?>
		  <input type="hidden" name="sem_name" value="<?php echo $result_para['sem_id']?>"/>
			<?php //echo sem_name($result_para['sem_id']);?>
		  	</td>
		<td>&nbsp;</td>
		<td></td>
        <td>
		  <?php
			 $sel_div="select * from division_master ";
			 $res_div=mysqli_query($conn,$sel_div);
			
			 while($div_combo=mysqli_fetch_array($res_div))
			 {							
				$div_array[] = array('id' => $div_combo['id'],
									  'text' => $div_combo['division']);								  
			 }
			 
			// echo tep_draw_pull_down_menu('division',$div_array, $default,' tabindex="4" ');
	      ?>
		  <input type="hidden" name="division" value="<?php echo $result_para['division_id']?>"/>
			<?php //echo division_name($result_para['division_id']);?>
		</td>
		</tr>	
		<!-- FACULTY **************************** -->
        <tr>
          <td align="center"><h4>Faculty Name :</h4> </td>
          <td><label>
            <?php
			 //$sel_fac="select distinct fm.f_id, fm.f_name, fm.l_name from faculty_master as fm, subject_master as sm where fm.b_id='".$result_para['b_id']."' and sm.batch_id='".$result_para['batch_id']."' and fm.f_id=sm.f_id and sm.sem_id=".$result_para['sem_id'];
			 $sel_fac="select distinct f_id, f_name, l_name from faculty_master";
			 $res_fac=mysqli_query($conn,$sel_fac);

			 while($fac_combo=mysqli_fetch_array($res_fac))
			 {							
				$fac_array[] = array('id' => $fac_combo['f_id'],
									 'text' => $fac_combo['f_name'].'&nbsp;'.$fac_combo['l_name']);								  
			 }
			 $default = 1;
			 echo tep_draw_pull_down_menu('fac_name', $fac_array, $default,' tabindex="5" onChange="AjaxFunction(this.value);"');
			 
			 
			 ?>
			 </tr>
			 <tr>
          </label></td>
          <!-- <td>&nbsp;</td> -->
          <td align="center"><h4>Subject Taught :</h4> </td>
          <td><label>
            <?php
			 $sel_sub="select * from subject_master as sm , faculty_master as fm where fm.b_id='".$result_para['b_id']."' and fm.f_id=sm.f_id";
			 $res_sub=mysqli_query($conn,$sel_sub);
			
			 while($sub_combo=mysqli_fetch_array($res_sub))
			 {							
				$sub_array[] = array('id' => $sub_combo['sub_id'],
									  'text' => $sub_combo['sub_name']);								  
			 }
			 
			 echo tep_draw_pull_down_menu('sub_name',$sub_array,$default,' tabindex="6" ');
	      ?>
		  <!select name=sub_name>

          <!/select>
          </label></td>
        </tr>
		
                <tr></tr>
		<tr>
		<td colspan="5"  class="rounded-foot-left new" align="center" style="padding-left: 20px;"><center><input class="btn btn-info" type="submit" name="submit" value="Submit" tabindex="17"/></center></td>
				<td align="center" class="rounded-foot-right"></td>
                </tr>
		<tr>
          <td colspan="5" align="center"><h5>Average Rating from 0 to 9 given by students.</h5></td>
        </tr>
		<tr>
          <td colspan="5">
		  <table width="100%" id="rounded-corner" cellpadding="10" cellspacing="0" border="0" align="center">
		  <thead>
		  <tr >
		     <th width="8%" class="rounded-company" align="center">ID</th>			 
			 <th width="86%" class="rounded-q1" align="center">Questions</th>
			 <th width="6%" class="rounded-q4">&nbsp;</th>
		  </tr>
                      
		  </thead>
		  <?php
                        
                        if(isset($_POST['sub_name']) && isset($_POST['fac_name']) && isset($_POST['batch_name']))    
                        {   

													$testSql = "SELECT * FROM faculty_master where f_id='$_POST[fac_name]';";
												
													$testRes=mysqli_query($conn,$testSql);
													while($testRow=mysqli_fetch_assoc($testRes)){
														echo "<center><h4>Result For $testRow[f_name] $testRow[l_name]</h4></center>";
													}
													
											
                            $subject = $_POST['sub_name'];
                            $faculty = $_POST['fac_name'];
                            $batch = $_POST['batch_name'];
                            //echo $subject."one".$faculty."two".$batch."hereherehere";
                            $sql_que1="select * from feedback_ques_master";
                            $res_que1=mysqli_query($conn,$sql_que1);
                            $sql_avg="select avg(ans1) as a1,avg(ans2) as a2,avg(ans3) as a3,avg(ans4) as a4,avg(ans5) as a5,avg(ans6) as a6,avg(ans7) as a7,avg(ans8) as a8,avg(ans9) as a9 ,avg(ans10) as a10 from feedback_master where f_id='".$faculty."' AND sub_id='".$subject."' AND batch_id='".$batch."'";
                            $res_avg=mysqli_query($conn,$sql_avg);
                            $i=1;
                            $tab_ind=7;
                            $res_avg=mysqli_fetch_array($res_avg);
                            while($row_que1=mysqli_fetch_array($res_que1))
                            {
                                    echo "<tr>";
                                    echo "<td align=\"center\">".$i."</td>";
                                    echo "<td>".$row_que1['ques']."</td>";
                                    $col = 'a'.$i;
																		$fin=($res_avg[$col]/10)*100;
																		echo "<td>" .$fin."%"."</td>";$tab_ind++;
																		$tab_ind++;
                                    echo "</tr>";$i++;
                            }
                        
                            ?>
                      
                            <tr>
                            <td>Remarks</td>
                            <td colspan="2" style="width:605px; height:40px;"></td>
                            </tr>		  
                            <?php    
                            $sql_que="select * from feedback_master where f_id='".$faculty."' AND sub_id='".$subject."' AND batch_id='".$batch."'";
                            $res_que=mysqli_query($conn,$sql_que);
                            
                            $tab_ind=7;
                            if(mysqli_num_rows($res_que)>=1)
                            {
															$t=1;
                               // echo "hello";
                                while($row_que=mysqli_fetch_array($res_que))
                                {
																	if($row_que['remark']!=''){
																		echo "<tr>";
                                    echo "<td align=\"center\">".$t."</td>";
                                    echo "<td>".$row_que['remark']."</td>";
                                    echo "<td> </td>";$tab_ind++;
                                    echo "</tr>";$i++;$t++;
																	}
                                    
                                }
                            }
                        }
		  ?>		  
		  <tr>
		  <td></td>
		  <td colspan="2"><textarea name="remark" style="width:605px; height:40px; visibility: hidden" onkeypress="return isCharOnly(event);" tabindex="16"></textarea></td>
		  </tr>		  
		  				
		  </table>
    </form></td>
  </tr>
  <tr>
    <td width="697"  height="1"><?php ?></td>
  </tr>
  
</table>

<?php

if(isset($_POST['submit'])){
$sql001="SELECT * FROM feedback_master where average between 0 and 30 and f_id=$_POST[fac_name] and batch_id= '$_POST[batch_name]' and sub_id= '$_POST[sub_name]';";
$pqr1=mysqli_query($conn,$sql001);
$numRow1=mysqli_num_rows($pqr1);

$sql002="SELECT * FROM feedback_master where average between 31 and 70 and f_id=$_POST[fac_name] and batch_id= '$_POST[batch_name]' and sub_id= '$_POST[sub_name]';";
$pqr2=mysqli_query($conn,$sql002);
$numRow2=mysqli_num_rows($pqr2);

$sql003="SELECT * FROM feedback_master where average between 71 and 100 and f_id=$_POST[fac_name] and batch_id= '$_POST[batch_name]' and sub_id= '$_POST[sub_name]';";
$pqr3=mysqli_query($conn,$sql003);
$numRow3=mysqli_num_rows($pqr3);
$var1=0;$var2=0;$var3=0;
//$sql004="SELECT * FROM "
$result= $numRow1+$numRow2+$numRow3;
if($result!=0){
	$var1 = number_format((float)($numRow1/$result)*100, 2, '.', '');
	$var2 = number_format((float)($numRow2/$result)*100, 2, '.', '');
	$var3 = number_format((float)($numRow3/$result)*100, 2, '.', '');

	echo "
	<br><br>
	<div style='margin-left:30%; margin-right:30%;' >
	<div class='progress progress-striped'>
	<div class='progress-bar progress-bar-success' style='width:  $var3%'>
			<span class=sr-only></span><p style=color:black;>Excellent $var3% </p>
	</div>
	</div>
	<div class='progress progress-striped'>
	<div class='progress-bar progress-bar-warning' style='width:  $var2%'>
			<span class=sr-only></span><p style=color:black;>Good $var2% </p>
	</div>
	</div>
	<div class='progress progress-striped'>
	<div class='progress-bar progress-bar-danger' style='width:  $var1%'>
			<span class=sr-only></span><p style=color:black;>Average $var1% </p>
	</div>
	</div>
	
	</div>
	";
}


}

?>

</body>
</html>


<SCRIPT LANGUAGE="JavaScript">
<!-- Original:  Mikhail Esteves (miks80@yahoo.com) -->
<!-- Web Site:  http://www.freebox.com/jackol -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
var mikExp = /[$\\@\\!\\\#%\^\&\*\(\)\[\]\+\_\{\}\`\~\=\|]/;
function dodacheck(val) {
var strPass = val.value;
var strLength = strPass.length;
var lchar = val.value.charAt((strLength) - 1);
if(lchar.search(mikExp) != -1) {
var tst = val.value.substring(0, (strLength) - 1);
val.value = tst;
   }
}

//  End -->
</script>

<script language="javascript" type="text/javascript">
function isCharOnly(e)
{
	var unicode=e.charCode? e.charCode : e.keyCode
	//if (unicode!=8 && unicode!=9)
	//{ //if the key isn't the backspace key (which we should allow)
		 //disable key press
		if (unicode==45)
			return true;
		if (unicode>48 && unicode<57) //if not a number
			return false
	//}
}
function isNumberOnly(e)
{
	var unicode=e.charCode? e.charCode : e.keyCode
	if (unicode!=8 && unicode!=9)
	{ //if the key isn't the backspace key (which we should allow)
		 //disable key press
		//if (unicode==45)
		//	return true;
		if (unicode<48||unicode>57) //if not a number
			return false
	}
}
function chkForm(form)
{

		var RefForm = document.feedback_form;
		/*if (RefForm.roll_no.value.length != 11 )
		{
			alert("Enter Roll Number");	
			RefForm.roll_no.focus();				
			return false;
		}*/
		
		/*if (RefForm.date.value == '' )
		{
			alert("Enter Date");
			RefForm.date.focus();			
			return false;
		}
		if (RefForm.batch_name.value == 0 )
		{
			alert("Select Batch");
			RefForm.batch_name.focus();			
			return false;
		}
		if (RefForm.b_name.value == 0 )
		{
			alert("Select Branch");
			RefForm.b_name.focus();			
			return false;
		}
		if (RefForm.sem_name.value  == 0 )
		{
			alert("Select Semester");			
			RefForm.sem_name.focus();
			return false;
		}*/
		if (RefForm.fac_name.value == 0 )
		{
			alert("Select Faculty Name.");			
			RefForm.fac_name.focus();
			return false;
		}
		if (RefForm.sub_name.value == 0 )
		{
			alert("Select Subject");
			RefForm.sub_name.focus();			
			return false;
		}
		
		for(i=1;i<=9;i++)
		{
			if(eval("document.feedback_form.ans_"+i).value == '')
			{
				alert("Enter rating.");
				eval("document.feedback_form.ans_"+i).focus();	
				return false;
			}
			if(eval("document.feedback_form.ans_"+i).value > 9)
			{
				alert("Enter rating from 0 to 9.");
				eval("document.feedback_form.ans_"+i).focus();	
				return false;
			}
				
		}/**/
		
}
</script>
<?php
if(isset($_POST['logout'])){
	header("location: logoutHandler.php");
}
?>